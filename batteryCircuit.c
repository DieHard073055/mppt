/*	Solar Panel Battery Switching Circuit
	-------------------------------------
	By 	:	 Diehard of death 
	LMD 	:	 14/05/2015
*/


//Capture and average 10 samples of A0
//if A0 less than the thresh hold
//	Capture and average 10 samples of A1 and A2
//	Compare A1 and A2
//
//	if A2 > A1 && A2 > THRESHOLD
//		D6 = 1; delay 100ms
//		D9 = 1; delay 100ms
//		D8 = 1; delay 100ms
//		D7 = 1; delay 100ms

//	else if A1 > A2 && A1 > THRESHOLD
//		D7 = 0; delay 100ms
//		D8 = 0; delay 100ms
//		D9 = 0; delay 100ms
//		D6 = 0; delay 100ms

//	if A1 == A2 && A1 > THRESHOLD && A2 > THRESHOLD
//		D7 = 0; delay 100ms
//		D8 = 0; delay 100ms
//		D9 = 0; delay 100ms
//		D6 = 0; delay 100ms	

//PIN DEFINITIONS
//System pin connected to power input of the system
#define 	SYS_PIN 	A0

//Pins connected to power of the battery
#define 	BATT_1_PIN	A1
#define		BATT_2_PIN	A2

//Relay pins
#define		RELAY_1_PIN	9
#define		RELAY_2_PIN	8
#define		RELAY_3_PIN	7
#define 	RELAY_4_PIN	6

//Threshold values
#define 	SYS_THRESH	800
#define 	BATT_1_THRESH	000
#define		BATT_2_THRESH	000
#define 	BATT_1_HIGH	000
#define 	BATT_2_HIGH	000

//Samping
#define 	SAMPLING_RATE	10

//DEBUGING
#define 	DEBUG		true
#define 	DEBUGGING	if(DEBUG)
//Variables
int sys_value = 0;
int bat_1_value = 0;
int bat_2_value = 0;

void setup(){
	//Initialize Serial Communication 
	#ifdef DEBUG
	Serial.begin(9600);
	println_d("setup() : Serial Communication Initiated!");
	#endif
	//Initialize the relays
	init_relays();
	println_d("setup() : Relay Outputs Initialized!");
	
	//Initial relay setting
	println_d("setup() : Initiating Initial Relay Settings!");
	relay_setting_1();
}

void loop(){
	update_analog_values();
	if(sys_value < SYS_THRESH){
		if(bat_2_value > BATT_2_THRESH){	relay_setting_2(); }
		else if(bat_1_value > BATT_1_THRESH){	relay_setting_1(); }
		else if(bat_1_value > bat_2_value){	relay_setting_1(); }
		else{					relay_setting_2(); }
	}
}
void init_relays(){
	println_d("init_relays() : Settings all relay output pins to OUTPUT");
	pinMode(RELAY_1_PIN, OUTPUT);
	pinMode(RELAY_2_PIN, OUTPUT);
	pinMode(RELAY_3_PIN, OUTPUT);
	pinMode(RELAY_4_PIN, OUTPUT);
}
void relay_setting_1(){
	println_d("relay_setting_1(): Setting relays 3, 2, 1, 4 LOW");
	set_low(RELAY_3_PIN);	d_100();
	set_low(RELAY_2_PIN);	d_100();
	set_low(RELAY_1_PIN);	d_100();
	set_low(RELAY_4_PIN);	d_100();
}

void relay_setting_2(){
	println_d("relay_setting_2(): Setting relays 4, 1, 2, 3 HIGH");
	set_high(RELAY_4_PIN);   d_100();
        set_high(RELAY_1_PIN);   d_100();
        set_high(RELAY_2_PIN);   d_100();
        set_high(RELAY_3_PIN);   d_100();
}
void update_analog_values(){
	println_d("update_analog_values(): updating sys_value, bat_1_value and bat_2_value");
	
	sys_value 	= get_sensor_value(SYS_PIN);
	bat_1_value	= get_sensor_value(BATT_1_PIN);
	bat_2_value 	= get_sensor_value(BATT_2_PIN);	
	
	print_d("update_analog_values() : SYS_PIN 	= ");
	println_d(String(sys_value));
	print_d("update_analog_values() : BATT_1_PIN 	= ");
	println_d(String(bat_1_value));
	print_d("update_analog_values() : BATT_2_PIN	= ");
	println_d(String(bat_2_value)); 
}
int get_sensor_value(int pin){
	print_d("get_sensor_value(): Getting the sensor value for pin :");
	println_d(String(pin));
	int value=0;
	for(int i=0; i<SAMPLING_RATE; i++){
		value += analogRead(pin); d_100();
	}
	return value / SAMPLING_RATE;
}
void d_100(){
	delay(100);
}
void set_high(int pin){
	digitalWrite(pin, HIGH);
}
void set_low(int pin){
	digitalWrite(pin, LOW);
}
void print_d(String d){
	#ifdef DEBUG
	Serial.print(d);
	#endif
}
void println_d(String d){
	#ifdef DEBUG
	Serial.println(d);
	#endif
}

