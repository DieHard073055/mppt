/*
 * PIN OUT LCD
 * VSS-> GND
 * VDD-> 2
 * V0 -> 3
 * RS -> 4
 * RW -> 5
 * E  -> 6
 * D4 -> 7
 * D5 -> 8
 * D6 -> 9
 * D7 -> 10
 * A  -> 11
 * K  -> 12
 */
#include <LiquidCrystal.h>

/*
  Display Settings Start
  **********************
*/
#define VDD 2
#define V0 3
#define RS 4
#define RW 5
#define E 6
#define D4 7
#define D5 8
#define D6 9
#define D7 10
#define _A 11
#define K 12


int value_vdd = 255;
int value_v0 = 150;
int value_a = 100;
LiquidCrystal disp(RS, E, D4, D5, D6, D7);

/*
  Display Settings End
  ********************
*/

/*
 * MPPT Stuff start
 */
//display
long sp_i=0;
long sp_v=0;
int pwm_f=0;
long mppt_i=0;
long mppt_v=0;
long mppt_p=0;
long mppt_i_old=0;
long mppt_v_old=0;
long mppt_p_old=0;
long mppt_di=0;
long mppt_dv=0;
long mppt_dp=0;

/*
 * MPPT Stuff end
 */
void setup() {
  TCCR5B = TCCR5B & B11111000 | B00000001;
  setup_display();
}

void loop() {
  get_readings();
  display_data();
  mppt();
}

void mppt(){
  if(!mppt_dv){
    if(!mppt_di){
      return;
    }else{
      if(mppt_di > 0) pwm_f++;
      else pwm_f--;
    }
  }else if(!(mppt_dp / mppt_dv)){
    return;
  }else{
    if((mppt_dp/mppt_dv) > 0) pwm_f++;
    else pwm_f--;
  }

}

void setup_display(){
  pinMode(VDD, OUTPUT);
  pinMode(V0, OUTPUT);
  pinMode(RW, OUTPUT);
  pinMode(_A, OUTPUT);
  pinMode(K, OUTPUT);

  digitalWrite(VDD, HIGH);
  analogWrite(V0, value_v0);
  digitalWrite(RW, LOW);
  analogWrite(_A, value_a);
  digitalWrite(K, LOW);

  disp.begin(20,4);
}

void display_data(){


  disp.setCursor(0,0);
  disp.print("spi:");
  disp.print(sp_i);
  disp.print(" spv:");
  disp.print(sp_v);
  disp.setCursor(0,1);
  disp.print("mpi:");
  disp.print(mppt_i);
  disp.print(" mpv:");
  disp.print(mppt_v);
  disp.setCursor(0,2);
  disp.print("pwm:");
  disp.print(pwm_f);

  delay(1000);
}
void get_readings(){
  mppt_i_old = mppt_i;
  mppt_v_old = mppt_v;
  mppt_p_old = mppt_p;
  sp_i = (long) analogRead(A0);
  sp_v = (long) analogRead(A1);
  mppt_i = (long) analogRead(A2);
  mppt_v = (long) analogRead(A3);
  mppt_p = mppt_v * mppt_i;
  mppt_di = mppt_i - mppt_i_old;
  mppt_dv = mppt_v - mppt_v_old;
  mppt_dp = mppt_p - mppt_p_old;
}
